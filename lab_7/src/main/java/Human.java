
public class Human extends Player{
	public Human(String name, int hp) {
		super(name,hp);
	}
	public void eat(Player eater,Player enemy) {
		if(eater.getAlive()&&enemy.getAlive()==false&&enemy instanceof Monster&&enemy.getBurnt()) {
			eater.getDiet().add(enemy);
			eater.setHp(eater.getHp()+15);
		}
	}
}
