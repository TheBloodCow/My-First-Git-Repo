
public class Magician extends Human{
	public Magician(String name, int hp) {
		super(name,hp);
	}
	public void burn(Player attacker,Player enemy) {
		if(attacker.getAlive()) {
			if(enemy.getHp()-10>0&&enemy instanceof Magician== false) {
				enemy.setHp(enemy.getHp()-10);
			}else if(enemy instanceof Magician &&enemy.getHp()-10>20){
				enemy.setHp(enemy.getHp()-20);
			}else {
				enemy.setHp(0);
				enemy.setBurnt(true);
			}
		}
		if(enemy.getHp()<=0) {
			enemy.setAlive(false);
			enemy.setBurnt(true);
		}else {
			enemy.setBurnt(true);
		}
	}
}
