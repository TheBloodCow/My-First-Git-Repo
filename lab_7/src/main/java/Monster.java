
public class Monster extends Player{
	public Monster(String name, int hp) {
		super(name,2*hp);
		this.setRoar("AAAAAAaaaAAAAAaaaAAAAAA");
	}
	public Monster(String name, int hp, String roar) {
		super(name,2*hp);
		this.setRoar(roar);
	}
}
