import java.util.*;
public class Player {
	private String name;
	private int hp;
	private ArrayList<Player> diet;
	private boolean alive;
	private boolean burnt;
	private String roar;
	
	public Player(String name, int hp) {
		this.name = name;
		this.hp = hp;
		if(hp>0) {
			this.alive=true;
		}else {
			this.alive=false;
		}
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getName() {
		return this.name;
	}
	public void setHp(int hp) {
		this.hp=hp;
	}
	public int getHp() {
		return this.hp;
	}
	public void setDiet(Player player) {
		this.diet.add(player);
	}
	public ArrayList<Player> getDiet() {
		return this.diet;
	}
	public void setBurnt(boolean burnt) {
		this.burnt = burnt;
	}
	public boolean getBurnt() {
		return this.burnt;
	}
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	public boolean getAlive() {
		return this.alive;
	}
	public String getRoar() {
		return this.roar;
	}
	public void setRoar(String roar) {
		this.roar = roar;
	}
	public void attack(Player attacker,Player enemy) {
		if(attacker.getAlive()) {
			if(enemy.getHp()-10>0&&enemy instanceof Magician== false) {
				enemy.setHp(enemy.getHp()-10);
			}else if(enemy instanceof Magician &&enemy.getHp()-10>20){
				enemy.setHp(enemy.getHp()-20);
			}else {
				enemy.setHp(0);
			}
		}
		if(enemy.getHp()<=0) {
			enemy.setAlive(false);
		}
	}
	public void eat(Player eater,Player enemy) {
		if(eater.getAlive()&&enemy.getAlive()==false) {
			eater.getDiet().add(enemy);
			eater.setHp(eater.getHp()+15);
		}
	}
}
