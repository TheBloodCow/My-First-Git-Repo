import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (int i=0; i<player.size(); i++) {
        	if(player.get(i).getName()==name) {
        		return player.get(i);
        	}
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(chara)) {
    			return "There is already a character named "+chara;
    		}
    	}
    	if(tipe.toLowerCase()=="human") {
    		player.add(new Human(chara, hp));
    	}else if(tipe.toLowerCase()=="monster") {
    		player.add(new Monster(chara,hp));
    	}else{
    		player.add(new Magician(chara,hp));
    	}
    	return chara+" is added to the game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(chara)) {
    			return "There is already a character named "+chara;
    		}
    	}
    	if(tipe.toLowerCase()=="human") {
    		return "Humans can't roar. Monsters only.";
    	}else if(tipe.toLowerCase()=="monster") {
    		player.add(new Monster(chara,hp));
    	}else{
    		return "Magicians can't roar either. Monsters only.";
    	}
    	return chara+" is added to the game";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(chara)) {
    			player.remove(i);
    			return chara+" has been removed from the game";
    		}
    	}
    	return chara+" not found.";
    }
    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(chara)) {
    			if(player.get(i) instanceof Human) {
    				if(player.get(i).getAlive()) {
    					return "Human "+chara+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Alive"+"\n"+"Ate "+player.get(i).getDiet();
    				}else {
    					return "Human "+chara+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Dead"+"\n"+"Ate "+player.get(i).getDiet();
    				}
    			}else if(player.get(i) instanceof Monster) {
    				if(player.get(i).getAlive()) {
    					return "Monster "+chara+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Alive"+"\n"+"Ate "+player.get(i).getDiet();
    				}else {
    					return "Monster "+chara+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Dead"+"\n"+"Ate "+player.get(i).getDiet();
    				}
    			}else {
    				if(player.get(i).getAlive()) {
    					return "Magician "+chara+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Alive"+"\n"+"Ate "+player.get(i).getDiet();
    				}else {
    					return "Magician "+chara+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Dead"+"\n"+"Ate "+player.get(i).getDiet();
    				}
    			}
    		}
    	}
    	return chara+" not found.";
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
    	String total = "";
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getAlive()) {
    			if(player.get(i).getDiet().isEmpty()) {
    				total+= "Monster "+player.get(i).getName()+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Alive"+"\n"+player.get(i).getName()+" Ate "+dietPrint(player.get(i))+"\n";
    			}else {
    				total+= "Monster "+player.get(i).getName()+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Alive"+"\n"+player.get(i).getName()+" Have not eaten anything"+"\n";
    			}
			}else{
				if(player.get(i).getDiet().isEmpty()) {
    				total+= "Monster "+player.get(i).getName()+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Dead"+"\n"+player.get(i).getName()+" Ate "+dietPrint(player.get(i))+"\n";
    			}else {
    				total+= "Monster "+player.get(i).getName()+"\n"+"HP: "+player.get(i).getHp()+"\n"+"Dead"+"\n"+player.get(i).getName()+" Have not eaten anything"+"\n";
    			}
			}
    	}
    	if(total.isEmpty()) {
    		return "No Players";
    	}
    	return total;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
    	String total1 = "";
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(chara)) {
    			total1+= chara+" ate ";
    			for(int j = 0; j<player.get(i).getDiet().size();j++) {
    				if(player.get(i).getDiet().get(j) instanceof Human) {
    					total1 += "Human " + player.get(i).getDiet().get(j).getName();
    				}else if(player.get(i).getDiet().get(j) instanceof Monster) {
    					total1 += "Monster " + player.get(i).getDiet().get(j).getName();
    				}else {
    					total1 += "Magician " + player.get(i).getDiet().get(j).getName();
    				}
    				if(j==player.get(i).getDiet().size()-1==false) {
    					total1+=", ";
    				}
    			}
    		}
    	}
    	if(total1.isEmpty()) {
    		return chara+" not found.";
    	}
    	return total1;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
    	String total1 = "";
    	for(int i =0;i<player.size();i++) {
    		for(int j = 0; j<player.get(i).getDiet().size();j++) {
    			if(player.get(i).getDiet().get(j) instanceof Human) {
    				total1 += "Human " + player.get(i).getDiet().get(j).getName();
    			}else if(player.get(i).getDiet().get(j) instanceof Monster) {
    				total1 += "Monster " + player.get(i).getDiet().get(j).getName();
    			}else {
    				total1 += "Magician " + player.get(i).getDiet().get(j).getName();
    			}
    			if(j==player.get(i).getDiet().size()-1==false) {
    				total1+=", ";
    			}
    		}
    	}
    	if(total1.isEmpty()) {
    	return "No characters are in the game.";
    	}
    	return total1;
    	}
    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(meName)&&player.get(i).getAlive()){
    				for(int j =0;j<player.size();j++) {
    					if(player.get(j).getName().equals(enemyName)){
    						if(player.get(j).getHp()-10>0&&player.get(j) instanceof Magician== false) {
    							player.get(j).setHp(player.get(j).getHp()-10);
    						}else if(player.get(j) instanceof Magician &&player.get(j).getHp()-10>20){
    							player.get(j).setHp(player.get(j).getHp()-20);
    						}else {
    							player.get(j).setHp(0);
    							player.get(j).setAlive(false);
    						}
    					}
    				}
    				return meName+" attacks "+enemyName;
    			}
    		}
    	return meName +" cannot attack.";
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(meName)&&player.get(i).getAlive()&&player.get(i)instanceof Magician){
    			for(int j =0;j<player.size();j++) {
    				if(player.get(j).getName().equals(enemyName)){
    					if(player.get(j).getHp()-10>0&&player.get(j) instanceof Magician== false) {
    						player.get(j).setHp(player.get(j).getHp()-10);
    						player.get(j).setBurnt(true);
    					}else if(player.get(j) instanceof Magician &&player.get(j).getHp()-10>20){
    						player.get(j).setHp(player.get(j).getHp()-20);
    						player.get(j).setBurnt(true);
    					}else {
    						player.get(j).setHp(0);
    						player.get(j).setBurnt(true);
    						player.get(j).setAlive(false);
    					}
    				}
    			}
    			return meName+" burns "+enemyName;
    		}
    	}
    	return meName +" cannot burn.";
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(meName)&&player.get(i).getAlive()){
    			for(int j =0;j<player.size();j++) {
    				if(player.get(j).getName().equals(enemyName)){
    					if(player.get(j).getAlive()==false&&dietCheck(player.get(i), enemyName)==false) {
    						if(player.get(i) instanceof Human&&player.get(j).getBurnt()==false&&player.get(j)instanceof Human) {
    							return "A human cannot eat this.";
    						}else {
    							player.get(i).setHp(player.get(i).getHp()+15);
    							player.get(i).getDiet().add(player.get(j));
    							return meName + " eats " + enemyName;
    						}
    					}
    					return "It's still alive. Kill it first.";
    				}
    			}
    		}
    	}
    	return enemyName + "not found.";
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
    	for(int i =0;i<player.size();i++) {
    		if(player.get(i).getName().equals(meName)&&player.get(i)instanceof Monster==false) {
    			return "This is no monster, it can't roar.";
    		}else if(player.get(i).getName().equals(meName)&&player.get(i)instanceof Monster) {
    			return player.get(i).getRoar();
    		}
    	}
    	return meName +" not found.";
    }
    public boolean dietCheck(Player player, String chara){
    	for(int i =0;i<player.getDiet().size();i++) {
    		if(player.getDiet().get(i).getName().equals(chara)) {
    			return true;
    			}
    		}
    	return false;
    	}
    public String dietPrint(Player player){
    	String total3 = "";
    	for(int i =0;i<player.getDiet().size();i++) {
    		total3+=player.getDiet().get(i).getName();
    		if(i==player.getDiet().size()-1) {
    			total3+=", ";
    		}else {
    			total3+="";
    		}
    		}
    	return total3;
    	}
	}