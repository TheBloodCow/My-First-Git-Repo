
public class Manusia {

	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	
	public Manusia(String nama, int umur, int uang) {
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
	}
	public Manusia(String nama, int umur) {
		this.nama = nama;
		this.umur = umur;
		this.uang = 500000;
		this.kebahagiaan = 50;
	}
	public int ambilUang() {
		return this.uang;
	}
	public int ambilUmur() {
		return this.umur;
	}
	public String ambilNama() {
		return this.nama;
	}
	public float ambilKebahagiaan() {
		return this.kebahagiaan;
	}
	public void UpdateUang (int uang){
		this.uang=uang;
	}
	public void UpdateKebahagiaan (float kebahagiaan){
		if (kebahagiaan < 0) {
            this.kebahagiaan = 0;
        } else if (kebahagiaan > 100) {
            this.kebahagiaan = 100;
        } else {
            this.kebahagiaan = kebahagiaan;
        }
	}
	public void beriUang(Manusia Receiver) {
		char AsciiTot[]= Receiver.ambilNama().toCharArray();
		int[] ValuesTot = new int [Receiver.ambilNama().length()];
		for (int i =0; i <= Receiver.ambilNama().length()-1; i++) {
			int AsciiVal = (int) AsciiTot[i];
			ValuesTot[i] = AsciiVal;
		}
		int CompileNumber = 0;
		for (int j = 0; j<= Receiver.ambilNama().length()-1; j++) {
			CompileNumber = CompileNumber+ ValuesTot[j];;
		}
		int FinalBill = CompileNumber*100;
		if (ambilUang()>FinalBill) {
			UpdateKebahagiaan (ambilKebahagiaan() + (float)(FinalBill/6000.0));
			Receiver.UpdateKebahagiaan (Receiver.ambilKebahagiaan() + (float) (FinalBill/6000.0));
			int newuang = Receiver.ambilUang() - FinalBill;
			Receiver.UpdateUang(newuang);
			System.out.println(ambilNama() + " memberi uang sebanyak " +  FinalBill +  " kepada " + Receiver.ambilNama() + ", mereka berdua senang :D");;
		}
			else {
				System.out.println(ambilNama() + " ingin memberi uang kepada " + Receiver.ambilNama() + " namun tidak memiliki cukup uang :'(");	
			}
	}
	public void beriUang(Manusia Receiver, int jumlah) {
		int CompileNumber = jumlah;
		int FinalBill = CompileNumber*100;
		if (ambilUang()>FinalBill) {
			UpdateKebahagiaan (ambilKebahagiaan() + (float)(FinalBill/6000.0));
			Receiver.UpdateKebahagiaan (Receiver.ambilKebahagiaan() + (float) (FinalBill/6000.0));
			int newuang = Receiver.ambilUang() - FinalBill;
			Receiver.UpdateUang(newuang);
			System.out.println(ambilNama() + " memberi uang sebanyak " +  FinalBill +  " kepada " + Receiver.ambilNama() + ", mereka berdua senang :D");
		}
			else {
				System.out.println(ambilNama() + " ingin memberi uang kepada " + Receiver.ambilNama() + " namun tidak memiliki cukup uang :'(");	
			}
	}
	public void bekerja (int durasi, int bebanKerja) {
		if (ambilUmur()>18) {
			int PotensiKerjaTotal = durasi * bebanKerja;
			if (PotensiKerjaTotal <= ambilKebahagiaan()) {
				int BebanKerjaTotal = durasi * bebanKerja;
				UpdateKebahagiaan(ambilKebahagiaan() - BebanKerjaTotal);
				UpdateUang(ambilUang() + BebanKerjaTotal * 10000);
				System.out.println(ambilNama() + " bekerja full time, total pendapatan : " +  (BebanKerjaTotal*10000));
		}
			else if (PotensiKerjaTotal > ambilKebahagiaan()) {
				int DurEx = (int) ambilKebahagiaan()/bebanKerja;
				int BebanKerjaTotal = DurEx * bebanKerja;
				UpdateUang(ambilUang() + BebanKerjaTotal*10000);
				UpdateKebahagiaan(ambilKebahagiaan()-BebanKerjaTotal);
				System.out.println(ambilNama() + " tidak bisa bekerja full time karena sedih, total pendapatan : " +  (BebanKerjaTotal*10000));
			}
			else {
				System.out.println(ambilNama() + " belum boleh bekerja karena masih dibawah umur ):");	
			}
	}
	}
	public void rekreasi (String namaTempat) {
		int Biaya = namaTempat.length()*10000;
		if (ambilUang()>Biaya) {
			UpdateUang(ambilUang() - Biaya);
			UpdateKebahagiaan(ambilKebahagiaan()+ namaTempat.length());
			System.out.println(ambilNama() + " berekreasi di" + namaTempat +", "+ambilNama() + " senang:)");
		} else {
			System.out.println(ambilNama() + " tidak mempunyai cukup uang untuk berekreasi di" + namaTempat +":'(");
		}
	}
	public void sakit (String namaPenyakit) {
		UpdateKebahagiaan(ambilKebahagiaan()-namaPenyakit.length());
		System.out.println(ambilNama() + " terkena penyakit " + namaPenyakit +":'(");
	}
	public String toString () {
        return "Nama\t\t: " + ambilNama() + "\nUmur\t\t: " + ambilUmur() + "\nUang\t\t: " + ambilUang() + "\nKebahagiaan\t: " + ambilKebahagiaan();
    }
}
