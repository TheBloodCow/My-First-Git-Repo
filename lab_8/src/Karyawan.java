
public class Karyawan {
	private int gaji;
	private String name;
	private int gajiStaff;
	private int gajiAmt;
	private Karyawan[] command = new Karyawan[10];
	public Karyawan(String name, int gaji, int gajiStaff) {
		this.name = name;
		this.gaji = gaji;
		this.gajiStaff = gajiStaff;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGaji() {
		return this.gaji;
	}
	public void setGaji(int gaji) {
		this.gaji = gaji;
	}
	public int getGajiStaff() {
		return this.gajiStaff;
	}
	public void setGajiStaff(int gajiStaff) {
		this.gajiStaff = gajiStaff;
	}
	public int getGajiAmt() {
		return this.gajiAmt;
	}
	public void setGajiAmt(int gaji) {
		this.gajiAmt = gaji;
	}
	public void incGajiAmt() {
		this.gajiAmt += 1;
	}
	public Karyawan[] getCommand() {
		return command;
	}
	public void STATUS() {
		System.out.format("\n%s %i", this.name, this.gaji);
	}
	public void TAMBAH_BAWAHAN(Karyawan karyawan) {
		for(int i=0;i<10;i++) {
			if(this.getCommand()[i] instanceof Karyawan==false) {
				this.getCommand()[i]=karyawan;
				i=9;
			}
		}
	}
	public String NaikGaji() {
		System.out.format("\n%s mengalami kenaikan gaji sebesar 10% dari %i menjadi %i", this.getName(), this.getGaji(), this.getGaji()+(this.getGaji()/10));
		incGajiAmt();
		if(this.getGajiAmt()==6) {
			this.setGaji(this.getGaji()+(this.getGaji()/10));
		}
		return "";
	}
}
