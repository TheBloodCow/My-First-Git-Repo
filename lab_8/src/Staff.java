
public class Staff extends Karyawan{
	public Staff(String name, int gaji, int gajiStaff) {
		super(name, gaji, gajiStaff);
	}
	public void TAMBAH_BAWAHAN(Karyawan karyawan) {
		for(int i=0;i<10;i++) {
			if(karyawan instanceof Manager || karyawan instanceof Staff){
				System.out.println("Anda tidak layak memiliki bawahan");
			}else if(this.getCommand()[i] instanceof Karyawan==false) {
				this.getCommand()[i]=karyawan;
				i=9;
			}
		}
	}
	public String NaikGaji() {
		System.out.format("\n%s mengalami kenaikan gaji sebesar 10% dari %i menjadi %i", this.getName(), this.getGaji(), this.getGaji()+(this.getGaji()/10));
		incGajiAmt();
		if(this.getGajiAmt()==6) {
			this.setGaji(this.getGaji()+(this.getGaji()/10));
		}
		if(this.getGaji()>this.getGajiStaff()) {
			System.out.format("\nSelamat, %s telah dipromosikan menjadi MANAGER\n", this.getName());
			return "naik";
		}
		return "";
	}
}
