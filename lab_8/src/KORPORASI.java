import java.util.*;
public class KORPORASI {
	private static ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
	public static void TAMBAH_KARYAWAN(String type, String name, int gaji, int gajiStaff) {
		if(NameCheck(name)) {
			System.out.format("\nKaryawan dengan nama %s telah terdaftar", name);
		}else{
			if(type.toLowerCase().equals("intern")) {
				karyawan.add(new Intern(name,gaji, gajiStaff));
			}else if(type.toLowerCase().equals("staff")) {
				karyawan.add(new Staff(name,gaji, gajiStaff));
			}else if(type.toLowerCase().equals("manager")) {
				karyawan.add(new Manager(name,gaji, gajiStaff));
			}else {
				System.out.println("Input tidak valid");
			}
		}
	}
	public static void TAMBAH_BAWAHAN(String boss, String bawahan) {
		if(NameCheck(boss)==false||NameCheck(bawahan)==false) {
			System.out.println("Nama tidak berhasil ditemukkan");
		}else if(karyawan.get(NameIndex(boss)) instanceof Karyawan && karyawan.get(NameIndex(bawahan)) instanceof Karyawan){
			karyawan.get(NameIndex(boss)).TAMBAH_BAWAHAN(karyawan.get(NameIndex(bawahan)));
		}else {
			System.out.println("Invalid Input.");
		}
	}
	public static void STATUS(String name) {
		if(NameCheck(name)){
			karyawan.get(NameIndex(name)).STATUS();
		}else {
			System.out.format("%s tidak berhasil ditemukan", name);
		}
	}
	public static boolean NameCheck(String name) {
		int value = 0;
		for(int i=0; i<karyawan.size();i++) {
			if(karyawan.get(i).getName().toLowerCase().equals(name.toLowerCase())) {
				value+=1;
			}
		}
		if(value>0) {
			return true;
		}else {
			return false;
		}
	}
	public static int NameIndex(String name) {
		int index = 0;
		for(int i=0; i<karyawan.size();i++) {
			if(karyawan.get(i).getName().toLowerCase().equals(name.toLowerCase())) {
				index = i;
			}
		}
		return index;
	}
	public static void GAJIAN(int gajiStaff) {
		for(int i=0; i<karyawan.size();i++) {
			karyawan.get(i).NaikGaji();
			if(karyawan.get(i).NaikGaji().toLowerCase().equals("naik")) {
				karyawan.add(new Manager(karyawan.get(i).getName(), karyawan.get(i).getGaji(), gajiStaff));
				karyawan.remove(i);
			}
		}
	}
}
