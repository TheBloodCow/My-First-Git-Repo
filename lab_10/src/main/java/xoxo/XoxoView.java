package xoxo;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Naufal
 */
public class XoxoView {

    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;


    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Main frame of GUI.
     */
    private JFrame frame;

    /**
     * Top panel of the GUI.
     */
    private JPanel topPanel;

    /**
     * Bottom panel of the GUI.
     */
    private JPanel bottomPanel;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {

        JPanel inptPanel = new JPanel();
        inptPanel.setLayout(new GridLayout(4, 1));


        JPanel messagePanel = new JPanel(new GridLayout(1,2));
        JLabel messageLabel = new JLabel("   Message: ");
        messageField = new JTextField(1000);
        messagePanel.add(messageLabel);
        messagePanel.add(messageField);
        inptPanel.add(messagePanel);


        JPanel keyPanel = new JPanel(new GridLayout(1,2));
        JLabel keyLabel = new JLabel("   Key: ");
        keyField = new JTextField(1000);
        JPanel button = new JPanel(new GridLayout(1, 2));
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        JPanel seedPanel = new JPanel(new GridLayout(1,2));
        JLabel seedLabel = new JLabel("   Seed: ");
        seedField = new JTextField(1000);

        seedPanel.add(seedLabel);
        seedPanel.add(seedField);
        inptPanel.add(seedPanel);
        keyPanel.add(keyLabel);
        keyPanel.add(keyField);
        inptPanel.add(keyPanel);
        button.add(encryptButton);
        button.add(decryptButton);


        JPanel mainPanel = new JPanel(new BorderLayout());
        inptPanel.add(button);
        mainPanel.add(inptPanel, BorderLayout.NORTH);
        logField = new JTextArea("Log Activity:\n");
        mainPanel.add(logField, BorderLayout.CENTER);


        JFrame frame = new JFrame("Lab 10");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(mainPanel);
        frame.setSize(750, 600);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     *
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     *
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     *
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    /**
     * Method to set warning to GUI if it catch exceptions.
     * @param warning the message that is going to pop-out
     */
    public void setWarning(String warning) {
        JOptionPane.showMessageDialog(frame, warning);
    }

}
