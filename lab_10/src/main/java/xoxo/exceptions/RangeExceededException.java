package xoxo.exceptions;

/**
 * this exception is to catch whether it exceeded the range.
 *
 * @author Naufal
 */
public class RangeExceededException extends RuntimeException {

    //TODO: Implement the exception
    /**
     *constructor
     */
    public RangeExceededException(String message){
        super(message);
    }
}
