package xoxo.exceptions;

/**
 * This exception is to catch whether the request size too large
 *
 * @author Naufal
 */
public class SizeTooBigException extends RuntimeException {

    //TODO: Implement the exception
    public SizeTooBigException(String message) {
        super(message);
    }
}

