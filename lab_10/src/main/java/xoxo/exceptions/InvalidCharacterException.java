package xoxo.exceptions;

/**
 * this is the exception class that catches error about invalid character
 * @author Naufal
 *
 */
public class InvalidCharacterException extends RuntimeException {

    //TODO: Implement the exception
    public InvalidCharacterException(String message){
        super(message);
    }
}
