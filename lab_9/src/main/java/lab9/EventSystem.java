package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
* Class representing event managing system
*/
public class EventSystem
{
    
    private ArrayList<Event> events;//eventlist
    
    private ArrayList<User> users;//userlist
    
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }//initializes
    
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        String[] startArr = startTimeStr.split("_");
        String[] startDate = startArr[0].split("-");
        String[] startTime = startArr[1].split(":");
        GregorianCalendar start = new GregorianCalendar(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1]) - 1,
                Integer.parseInt(startDate[2]), Integer.parseInt(startTime[0]), Integer.parseInt(startTime[1]),
                Integer.parseInt(startTime[2]));
        String[] endArr = endTimeStr.split("_");
        String[] endDate = endArr[0].split("-");
        String[] endTime = endArr[1].split(":");
        GregorianCalendar end = new GregorianCalendar(Integer.parseInt(endDate[0]), Integer.parseInt(endDate[1]) - 1,
                Integer.parseInt(endDate[2]), Integer.parseInt(endTime[0]), Integer.parseInt(endTime[1]),
                Integer.parseInt(endTime[2]));
        BigInteger cost = new BigInteger(costPerHourStr);
        if (start.compareTo(end) < 0 ) {
            if (getEvent(name) == null) {
                Event temp = new Event(name, start, end, cost);
                events.add(temp);
                return "Event " + name + " berhasil ditambahkan!";
            }
            return "Event " + name + " sudah ada!";
        }
        return "Waktu yang diinputkan tidak valid!";
    }
    
    public String addUser(String name)
    {
        if ( getUser(name) == null) {
            User temp = new User(name);
            users.add(temp);
            return "User " + name + " berhasil ditambahkan!";
        }
        return "User " + name + " sudah ada!";
    }
    
    public String registerToEvent(String userName, String eventName)
    {
        User user = getUser(userName);
        Event event = getEvent(eventName);
        boolean isRegisterable = true;

        if ( user != null || event != null){
            if ( user != null){
                if (event != null){
                    for (Event e : user.getEvents()) {
                        isRegisterable &= event.isCompatible(e);
                    }
                    if ( isRegisterable) {
                        user.addEvent(event);
                        return userName + " berencana menghadiri " + eventName + "!";
                    }
                    return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
                }
                return " Tidak ada acara dengan nama " + eventName + "!";
            }
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }
        return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
    }

    public Event getEvent(String name){
        for (Event e : events){
            if(e.getName().equals(name)){
                return e;
            }
        }
        return null;
    }

    public User getUser(String name){
        for (User u : users){
            if(u.getName().equals(name)){
                return u;
            }
        }
        return null;
    }
}