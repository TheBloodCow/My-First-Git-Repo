import java.util.Scanner;
public class RabbitHouse {
	public static void main(String []args) {
		Scanner input = new Scanner(System.in);
		String InitInput = input.nextLine();
		String[] Splitted = InitInput.split(" ");
		String ModusOperand = Splitted[0];
		String RabbitName = Splitted[1];
		int RabbitNameLength = RabbitName.length();
		if (RabbitNameLength <= 10) {
			int RabbitCount = HeadCounter(RabbitNameLength, 1);
			input.close();
			System.out.println(RabbitCount);
		} else {
			System.out.println("No number above 10");
		}
	}
	public static int HeadCounter(int rabbits, int RabbitAmt) {
		int RabbitName = rabbits;
		int InitRabbit = RabbitAmt;
		if(RabbitName == 1) {
			return 1;
		} else {
			return RabbitName*InitRabbit+(HeadCounter(RabbitName-1, RabbitAmt*RabbitName));
		}
	}

}
