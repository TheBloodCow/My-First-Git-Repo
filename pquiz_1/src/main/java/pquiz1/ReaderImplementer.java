import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import enrollment.Student;
import reader.FileReader;
import enrollment.Course;

public class ReaderImplementer implements reader.CsvReader{
    public List<Student> getStudents() {
    	ArrayList<Student> output = new ArrayList<Student>();
    	for (int i = 0; i<FileReader.output2.size();i++) {
    		output.add(FileReader.output2.get(i));
    	}
    	return output;
    }
    public List<Student> getStudentsEnrolledInCourse(String courseName){
    	ArrayList<Student> output = new ArrayList<Student>();
    	for (int i = 0;i<FileReader.output2.size();i++) {
    		for (int j=0;j<FileReader.output2.get(i).getEnrolledCourse().size();j++) {
    			if(FileReader.output2.get(i).getEnrolledCourse().get(j).getName().equals(courseName)&&output.contains(FileReader.output2.get(i))==false) {
    				output.add(FileReader.output2.get(i));
    			}
    		}
    	}
    	return output;
    }
    public List<Student> getStudentsByEnrollYear(int year){
    	ArrayList<Student> output = new ArrayList<Student>();
    	for (int i = 0;i<FileReader.output2.size();i++) {
    		if(FileReader.output2.get(i).getEnrollYear()==year&&output.contains(FileReader.output2.get(i))==false) {
    			output.add(FileReader.output2.get(i));
    		}
    	}
    	return output;
    }
}
