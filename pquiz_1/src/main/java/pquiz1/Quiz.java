import enrollment.Student;
import reader.CsvReader;
import reader.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * This class is the main program for programming quiz 1.
 *
 * AMAL ADIGUNA 1706018901
 */
public class Quiz{

    private static String cwd ="C:\\Users\\AMAL ADIGUNA\\Documents\\All My FILES\\Working Place\\Java werk\\Quiz\\data\\uni-x-students.csv\\";

    public static void main(String[] args) throws IOException {
        // Create a reference to the data file as instance of Path object
        
        /* TODO: Create an instance of class that implements CsvReader and
           reads the CSV data from the file referred by dataFile */
        CsvReader reader = new ReaderImplementer();
        FileReader.getList(cwd);
        
        List<Student> students = reader.getStudents();
        System.out.println(String.format("There are %d unique students in the records",
                students.size()));

        List<Student> studentsWhoTakeKimiaDasar = reader.getStudentsEnrolledInCourse("Kimia Dasar");
        System.out.println(String.format("There are %d unique students that are taking Kimia Dasar",
                studentsWhoTakeKimiaDasar.size()));

        List<Student> studentsEnrolledIn2014 = reader.getStudentsByEnrollYear(2014);
        System.out.println(String.format("There are %d unique students that enrolled in 2014",
                studentsEnrolledIn2014.size()));

        // Reserved for demo with TA/lecturer

        // End of reserved section
    }
}

