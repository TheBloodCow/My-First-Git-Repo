package reader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import enrollment.Course;
import enrollment.Student;

public class FileReader {
	public static String output = "";
	public static ArrayList<String> output1 = new ArrayList<String>();
	public static ArrayList<Student> output2 = new ArrayList<Student>();
	public static ArrayList<ArrayList<String>> output3 = new ArrayList<ArrayList<String>>();
	public static void getList(String input) throws FileNotFoundException{
		Scanner scanner = new Scanner(new File(input));
        while(scanner.hasNextLine()){
            output+=scanner.nextLine()+",";
        }
        String[] split = output.split(",");
        for (int i = 0; i<split.length;i++) {
        	output1.add(split[i]);
        }
        ArrayList<ArrayList<String>> output = new ArrayList<ArrayList<String>>();
		for(int i=0; i<output1.size();i+=6) {
			ArrayList<String> tempy = new ArrayList<String>();
			for (int j = 0; j<6; j++) {
				tempy.add(output1.get(i+j));
			}
			output3.add(tempy);
		}
		for(int i = 0; i<output3.size();i++) {
			int index;
			Course newCourse = new Course(output.get(i).get(3),output.get(i).get(4),Integer.parseInt(output.get(i).get(5)));
			ArrayList<Course> newCourseList = new ArrayList<Course>();
			newCourseList.add(newCourse);
			Student newStudent = new Student(output.get(i).get(0), Integer.parseInt(output.get(i).get(1)), output.get(i).get(2), newCourseList);
			if(output2.contains(newStudent)) {
				index = output2.indexOf(newStudent);
				output2.get(index).addCourse(newCourse);
			}else {
				output2.add(newStudent);
			}
		}
		scanner.close();
	}
}
